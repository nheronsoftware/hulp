// Copyright (c) 2007 Sun Microsystems
//    
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//    
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//    
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

package net.java.hulp.i18n;

/**
* A logger that exposes the same "interface" as the JDK logger but requires localized
* strings.
*
* @author Frank Kieviet
* @version $Revision: 1.3 $
*/
public final class Logger {
   private final java.util.logging.Logger mDelegate;

   private Logger(java.util.logging.Logger delegate) {
       mDelegate = delegate;
   }

   /**
    * See {@link org.apache.log4j.Logger#getLogger}
    *
    * @param name name of the logger
    * @return Logger instance
    */
   public static Logger getLogger(String name) {
       return new Logger(java.util.logging.Logger.getLogger(name));
   }

   /**
    * See {@link org.apache.log4j.Logger#}
    *
    * @param clazz Class whose name is to be used as the logger name
    * @return Logger instance
    */
   public static Logger getLogger(Class clazz) {
       return getLogger(clazz.getName());
   }

   /**
    * See {@link org.apache.log4j.Category#debug}
    *
    * @param message msg to be logged
    */
   public final void fine(Object message) {
       mDelegate.log(java.util.logging.Level.FINE,
           message == null ? null : message.toString());
   }

   /**
    * See {@link org.apache.log4j.Category#debug}
    *
    * @param message msg to be logged
    * @param t exception
    */
   public final void fine(Object message, Throwable t) {
       mDelegate.log(java.util.logging.Level.FINE,
           message == null ? null : message.toString(), t);
   }

   /**
    * See {@link org.apache.log4j.Category#debug}
    *
    * @param message msg to be logged
    */
   public final void finer(Object message) {
       mDelegate.log(java.util.logging.Level.FINER,
           message == null ? null : message.toString());
   }

   /**
    * See {@link org.apache.log4j.Category#debug}
    *
    * @param message msg to be logged
    * @param t exception
    */
   public final void finer(Object message, Throwable t) {
       mDelegate.log(java.util.logging.Level.FINER,
           message == null ? null : message.toString(), t);
   }

   /**
    * See {@link org.apache.log4j.Category#debug}
    *
    * @param message msg to be logged
    */
   public final void finest(Object message) {
       mDelegate.log(java.util.logging.Level.FINEST,
           message == null ? null : message.toString());
   }

   /**
    * See {@link org.apache.log4j.Category#debug}
    *
    * @param message msg to be logged
    * @param t exception
    */
   public final void finest(Object message, Throwable t) {
       mDelegate.log(java.util.logging.Level.FINEST,
           message == null ? null : message.toString(), t);
   }

   /**
    * See {@link org.apache.log4j.Category#error}
    *
    * @param message msg to be logged
    */
   public final void severe(LocalizedString message) {
       mDelegate.log(java.util.logging.Level.SEVERE,
           message == null ? null : message.toString());
   }

   /**
    * See {@link org.apache.log4j.Category#error}
    *
    * @param message msg to be logged
    * @param t exception to be logged
    */
   public final void severe(LocalizedString message, Throwable t) {
       mDelegate.log(java.util.logging.Level.SEVERE,
           message == null ? null : message.toString(), t);
   }

   /**
    * See {@link org.apache.log4j.Category#error}
    *
    * @param message msg to be logged
    * @param t exception to be logged
    */
   public final void errorNoloc(String message, Throwable t) {
       mDelegate.log(java.util.logging.Level.SEVERE,
           message == null ? null : message.toString(), t);
   }

   /**
    * See {@link org.apache.log4j.Category#info}
    *
    * @param message msg to be logged
    */
   public final void info(LocalizedString message) {
       mDelegate.log(java.util.logging.Level.INFO,
           message == null ? null : message.toString());
   }

   /**
    * See {@link org.apache.log4j.Category#info}
    *
    * @param message msg to be logged
    */
   public final void infoNoloc(String message) {
       mDelegate.log(java.util.logging.Level.INFO,
           message == null ? null : message.toString());
   }

   /**
    * See {@link org.apache.log4j.Category#info}
    *
    * @param message msg to be logged
    * @param t exception to be logged
    */
   public final void infoNoloc(String message, Throwable t) {
       mDelegate.log(java.util.logging.Level.INFO,
           message == null ? null : message.toString(), t);
   }

   /**
    * See {@link org.apache.log4j.Category#info}
    *
    * @param message msg to be logged
    * @param t exception to be logged
    */
   public final void info(LocalizedString message, Throwable t) {
       mDelegate.log(java.util.logging.Level.INFO,
           message == null ? null : message.toString(), t);
   }

   /**
    * See {@link org.apache.log4j.Category#isDebugEnabled}
    *
    * @return if debug logging is enabled
    */
   public final boolean isFine() {
       return mDelegate.isLoggable(java.util.logging.Level.FINE);
   }

   /**
    * See {@link org.apache.log4j.Category#warn}
    *
    * @param message msg to be logged
    */
   public final void warn(LocalizedString message) {
       mDelegate.log(java.util.logging.Level.WARNING,
           message == null ? null : message.toString());
   }

   /**
    * See {@link org.apache.log4j.Category#warn}
    *
    * @param message msg to be logged
    * @param t exception to be logged
    */
   public final void warn(LocalizedString message, Throwable t) {
       mDelegate.log(java.util.logging.Level.WARNING,
           message == null ? null : message.toString(), t);
   }

   /**
    * See {@link org.apache.log4j.Category#warn}
    *
    * @param message msg to be logged
    */
   public final void warnNoloc(String message) {
       mDelegate.log(java.util.logging.Level.WARNING,
           message == null ? null : message.toString());
   }

   /**
    * See {@link org.apache.log4j.Category#getName}
    *
    * @return String
    */
   public final String getName() {
       return mDelegate.getName();
   }

   /**
    * See {@link org.apache.log4j.Category#getLevel}
    *
    * @return Level
    */
   public final java.util.logging.Level getLevel() {
       return mDelegate.getLevel();
   }

   /**
    * See {@link org.apache.log4j.Category#isEnabledFor}
    *
    * @param level msg to be logged
    * @return boolean
    */
   public final boolean isLoggable(java.util.logging.Level level) {
       return mDelegate.isLoggable(level);
   }

   /**
    * See {@link org.apache.log4j.Category#isInfoEnabled}
    *
    * @return boolean
    */
   public final boolean isInfo() {
       return mDelegate.isLoggable(java.util.logging.Level.INFO);
   }

   /**
    * See {@link org.apache.log4j.Category#setLevel}
    *
    * @param level msg to be logged
    */
   public final void setLevel(java.util.logging.Level level) {
       mDelegate.setLevel(level);
   }
}

// Copyright (c) 1999 Frank Gerard
//    
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//    
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//    
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

package net.sf.hulp.util;

import java.text.Format;
import java.text.SimpleDateFormat;

/**
 * A collection of string tools
 */
public class StringTools {

    /**
     * Returns a string representation of the specified time (in milliseconds after 70).
     * Mainly used for debugging (doesn't take a locale!)
     */
    public static String timeString(long ms70) {
        String ret = null;
        if (ms70 == 0) {
            ret = "-";
        } else {
            java.util.Date d = new java.util.Date(ms70);
            Format fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            ret = fmt.format(d);
        }
        return ret;
    }

    /**
     * Returns if a string is null or empty
     */
    public static boolean empty(String s) {
        return s == null || s.length() == 0;
    }
}
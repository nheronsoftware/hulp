// Copyright (c) 2007 Sun Microsystems
// Copyright (c) 1999 Frank Gerard
//    
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//    
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//    
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

package net.sf.hulp.profiler;

import net.java.hulp.measure.internal.FactoryFactoryV2;
import net.java.hulp.measure.internal.FactoryV2;

/**
 * Creates factories for creating measurements
 */
public class FactoryFactory implements net.sf.hulp.measure.Measurement.FactoryFactory, FactoryFactoryV2 {
    /**
     * Indicates if the Meaurement.Factory should be loaded
     */
    public static final String ISON = "net.sf.hulp.profiler";
    
    /**
     * @return true if profiling should be turned on
     */
    public boolean isOn() {
        String ison = System.getProperty(ISON, "false");
        return ison.equalsIgnoreCase("true") || ison.equals("1");
    }

    /**
     * @see net.sf.hulp.measure.Measurement.FactoryFactory#newFactoryV2()
     */
    public net.sf.hulp.measure.Measurement.Factory newFactory() {
        if (isOn()) {
            return RealProfiler.get();
        } else {
            throw new RuntimeException("Profiling turned off");
        }
    }

    /**
     * @see net.java.hulp.measure.Probe.FactoryFactoryV2#newAlwaysOnFactoryV2()
     */
    public FactoryV2 newAlwaysOnFactoryV2() {
        return RealProfiler.get();
    }

    /**
     * @see net.sf.hulp.measure.Measurement.FactoryFactory#newFactoryV2()
     */
    public FactoryV2 newFactoryV2() {
        if (isOn()) {
            return RealProfiler.get();
        } else {
            throw new RuntimeException("Profiling turned off");
        }
    }
}

// Copyright (c) 1999 Frank Gerard
//    
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//    
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//    
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

package net.sf.hulp.profiler;

import net.sf.hulp.util.Markup;

import javax.management.openmbean.TabularData;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * A profiler implementation that doesn't do anything and hence doesn't take any
 * overhead.
 */
public class VoidProfiler extends Profiler {
    /**
     * @see net.sf.hulp.measure.Measurement.Factory#create(java.lang.String, java.lang.String)
     */
    public net.sf.hulp.measure.Measurement create(String topic, String subtopic) {
        return null;
    }

    /**
     * @see net.sf.hulp.profiler.Profiler#dump(java.io.PrintWriter, net.sf.hulp.util.Markup)
     */
    public void dump(final PrintWriter out, final Markup f) throws IOException {
        out.println("The profiler is not active; change system property to select a"
                    +" a measuring profiler");
    }

    /**
     * @see net.sf.hulp.profiler.Profiler#clear()
     */
    public void clear() {}

    /**
     * @see net.sf.hulp.profiler.Profiler#dump()
     */
    public Map[] dump() {
        return new Map[0];
    }
    
    /**
     * @see net.sf.hulp.profiler.Profiler#dump(java.lang.String)
     */
    public Map dump(String topicSubtopicSeparator) {
        return new HashMap();
    }
    
    public void help(PrintWriter out, Markup f) {
        out.println("The profiler is not enabled. Set the following argument to the JVM:<br>");
        out.println(f.beginPre() + "-Dnet.sf.hulp.profiler=1" + f.endPre() + "<BR");
    }

    /**
     * @see net.sf.hulp.measure.Measurement.Factory#getData(java.util.List)
     */
    public TabularData getData(List<Pattern[]> criteria) {
        return null;
    }

    /**
     * @see net.java.hulp.measure.Probe.FactoryV2#createV2(java.lang.String, java.lang.String)
     */
    public net.java.hulp.measure.Probe createV2(int level, Class source, String topic, String subtopic) {
        return null;
    }

    public void clearData(List<Pattern[]> criteria) {
    }
}
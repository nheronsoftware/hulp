// Copyright (c) 1999 Frank Gerard
//    
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//    
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//    
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

package net.sf.hulp.profiler;

import net.sf.hulp.util.Markup;
import net.sf.hulp.util.MarkupHtml;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Exposes accumulated measurement data in a servlet
 */
public class ProfilerServlet extends HttpServlet {

    private static final long serialVersionUID = 1777967092267346221L;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws
        IOException, ServletException {
        MarkupHtml f = new MarkupHtml();

        if (request.getQueryString() == null) {
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            out.print(f.pageFrame());
        } else if ("menu".equals(request.getQueryString())) {
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            out.println(new MarkupHtml.Menu("Profiler")
                .add("?cmd=dump", "Dump")
                .add("?cmd=dumptext", "Dump text")
                .add("?cmd=clear", "Clear")
                .add("?cmd=help", "Help")
                );
        } else if ("body".equals(request.getQueryString()) || "help".equals(request.getParameter("cmd"))) {
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            out.println(f.beginPage());
            Profiler.get().help(out, f);
            out.println(f.endPage());
        } else if ("dump".equals(request.getParameter("cmd"))) {
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            out.println(f.beginPage());
            dump(out, f);
            out.println(f.endPage());
        } else if ("dumptext".equals(request.getParameter("cmd"))) {
            response.setContentType("text/plain");
            PrintWriter out = response.getWriter();
            Markup ftext = new Markup();
            dump(out, ftext);
        } else if ("clear".equals(request.getParameter("cmd"))) {
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            out.println(f.beginPage());
            out.println(f.beginArea() + "This will clear all accumulated measurements.");
            out.println("<br><br>Proceed? ");
            out.println("<a href='?cmd=wipe'>Yes</a>");
            out.println(f.endPage());
        } else if ("wipe".equals(request.getParameter("cmd"))) {
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            out.println(f.beginPage());
            clear(out, f);
            out.println(f.endPage());
        } else {
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            out.println(f.beginPage());
            out.println("Unknown request " + request.getRequestURL() + " ... " +
                request.getQueryString());
            out.println(f.endPage());
        }
    }

    public void dump(PrintWriter out, Markup f) throws IOException,
        ServletException {
        out.println(f.beginTable(Markup.TABLESTYLE_BANNEREGAL));
        Profiler.get().dump(out, f);
        out.println(f.endTable());
    }

    public void clear(PrintWriter out, Markup f) throws IOException,
        ServletException {
        Profiler.get().clear();
        out.println("Cleared");
    }
}

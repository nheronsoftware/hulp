// Copyright (c) 1999 Frank Gerard
//    
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//    
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//    
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

package net.sf.hulp.measure;

/**
 * To measure time intervals such as functions and accumulate these
 * measurements a profiler package can be used. The profiler package
 * is a plug in; the plug in will be used if it is there; if it is
 * not found, there will be minimal overhead due to measuring since
 * no measurements are taking place.<br><br>
 *
 * This class represents a single measurement. Use the factory method
 * begin() to create a new measurement. Typical usage is:<br><br>
 *
 * <code>
 * Measurement m = Measurement.begin(topic);<br>
 * ... do something ...<br>
 * d.end();<br>
 * </code>
 * <br><br>
 *
 * Notes:<br>
 * - A measurement has a topic and a subtopic.
 * - Topic and sub topic can be set afterwards.
 * - A measurement is not reusable.
 */
public class Measurement {

    /**
     * Creates a factory
     */
    public interface FactoryFactory {
        Factory newFactory();
    }

    /**
     * Creates a measurement
     */
    public interface Factory {
        Measurement create(String topic, String subtopic);
    }

    private static Factory s_Factory;
    private static Measurement s_voidMeasurement;
    private static final String FACTORYNAME = "net.sf.hulp.profiler.FactoryFactory";

    /**
     * Constructor; forces callers to use factory method begin()
     */
    protected Measurement() {
    }

    /**
     * Factory method: returns a new (when measuring) or no-op Measurement (when not
     * measuring)
     */
    public static Measurement begin(String topic) {
        return begin(topic, null);
    }

    // Bootstrap: ensures that the begin() factory method will return a void or
    // concrete measurement
    static {
        try {
            // Try to load factory
            String name = System.getProperty(FACTORYNAME, FACTORYNAME);
            Class c = Class.forName(name);
            FactoryFactory f = (FactoryFactory) c.newInstance();
            s_Factory = f.newFactory();
            System.out.println("Measurement factory loaded: " + name + " (" + s_Factory + ")");
        } catch (Throwable ex) {
            // Ignore error
        }

        // Factory failed to load? Disable measurements
        if (s_Factory == null) {
            s_voidMeasurement = new Measurement();
        }
    }

    /**
     * Factory method: returns a new (when measuring) or no-op Measurement (when not
     * measuring)
     */
    public static Measurement begin(String topic, String subTopic) {
        if (s_voidMeasurement != null) {
            return s_voidMeasurement;
        } else {
            return s_Factory.create(topic, subTopic);
        }
    }

    /**
     * Returns if there is a measuring infrastructure installed
     * @return true if is installed
     */
    public static boolean isInstalled() {
        return s_voidMeasurement == null;
    }

    /**
     * Ends the time interval and adds the measurement to the profiler list.
     * This method is overridden in the case a profiler plug-in is used.
     */
    public void end() {

    }

    /**
     * Resets the topic of the measurement
     * This method is overridden in the case a profiler plug-in is used.
     */
    public void setTopic(String topic) {

    }

    /**
     * Further specifies the measurement
     * This method is overridden in the case a profiler plug-in is used.
     */
    public void setSubtopic(String subTopic) {

    }
}

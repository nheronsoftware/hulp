// Copyright (c) 1999 Frank Gerard
//    
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//    
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//    
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

package net.sf.hulp.measure;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Can be used as an insertion between an interface and an implementation object;
 * this measures all calls on the interface.
 */
public class ItfMeasurement implements InvocationHandler {
    private Object m_target;
    private String m_name;

    /**
     * Use getItfMeasurement to construct
     */
    private ItfMeasurement(Object target) {
        m_target = target;
        m_name = target.getClass().getName();
    }

    /**
     * Returns an object that implements the specified interface; hence the
     * returned object can be cast into an object of the specified interface.
     * <br><br>
     *
     * Example:<br>
     * <code>
     * Connection con = (Connection) getItfMeasurement.getItfMeasurement(log, getConnection(), Connection.class);
     * </code>
     */
    public static Object getItfMeasurement(Object implementation, Class interface_) {
        if (!Measurement.isInstalled()) {
            return implementation;
        } else {
            return Proxy.newProxyInstance(implementation.getClass().getClassLoader()
                    , new Class[] { interface_ }
                    , new ItfMeasurement(implementation)
                            );
        }
    }

    /**
     * Required by the InvocationHandler interface; called on each invocation of the
     * interface. Times the call and delegates to the implementation.
     */
    public Object invoke(Object proxy, Method method_, Object[] args) throws Throwable {
        Object ret = null;
        Measurement measurement = Measurement.begin(m_name, method_.getName());
        try {
            ret = method_.invoke(m_target, args);
        } catch (InvocationTargetException e) {
            throw e.getTargetException();
        } finally {
            measurement.end();
        }
        return ret;
    }
}